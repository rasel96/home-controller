package com.rasel96.homecontroller;

public class ElectronicItem {

    private boolean onState = false;

    public boolean isOnState() {
        return onState;
    }

    public void setState(boolean onState) {
        this.onState = onState;
    }
}
