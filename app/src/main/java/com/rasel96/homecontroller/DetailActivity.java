package com.rasel96.homecontroller;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        // Back Button on the TOP
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Getting Data from prev Activity
        int id;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                id = 0;
            } else {
                id = extras.getInt("id");
            }
        } else {
            id = (int) savedInstanceState.getSerializable("id");
        }

        // Declaring the bottom Nav Bar
        BottomNavigationView bottomNav = findViewById(R.id.bottom_nav);
        bottomNav.setOnNavigationItemSelectedListener(navListener);

        // Setting the default
        bottomNav.setSelectedItemId(startingUP(id));

    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                    Fragment selectedFragment = null;

                    switch (menuItem.getItemId()){

                        case R.id.light_bulb_nav:
                            selectedFragment = new LightBulbFragment();
                            break;
                        case R.id.fan_nav:
                            selectedFragment = new FanFragment();
                            break;
                        case R.id.door_nav:
                            selectedFragment = new DoorFragment();
                            break;
                    }

                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                            selectedFragment).commit();
                    return true;
                }
            };


    private int startingUP(int id){

        Fragment selectedFragment = null;
        int navId = 0;

        switch (id){

            case R.id.lightBulbCardView:
                selectedFragment = new LightBulbFragment();
                navId = R.id.light_bulb_nav;
                break;
            case R.id.fanCardView:
                selectedFragment = new FanFragment();
                navId = R.id.fan_nav;
                break;
            case R.id.doorCardView:
                selectedFragment = new DoorFragment();
                navId = R.id.door_nav;
                break;
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                selectedFragment).commit();
        return navId;
    }
}
