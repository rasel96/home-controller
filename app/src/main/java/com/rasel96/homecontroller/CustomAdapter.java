package com.rasel96.homecontroller;

import android.content.Context;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;

public class CustomAdapter extends ArrayAdapter<DataModel> {

    private ArrayList<DataModel> dataSet;
    Context mContext;

    public CustomAdapter(ArrayList<DataModel> data, Context context) {

        super(context, R.layout.list_item_layout, data);
        this.dataSet = data;
        this.mContext = context;

    }

    public static class ViewHolder{
        ImageView imageView;
        TextView name, status;
        Switch aSwitch;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        DataModel dataModel = getItem(position);

        ViewHolder viewHolder;

        final View result;

        if (convertView == null){

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.list_item_layout, parent, false);

            viewHolder.name = convertView.findViewById(R.id.name_list_item_text_view);
            viewHolder.status = convertView.findViewById(R.id.status_list_item_text_view);
            viewHolder.imageView = convertView.findViewById(R.id.img_list_item);
            viewHolder.aSwitch = convertView.findViewById(R.id.switch_list_item);

            result = convertView;
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) convertView.getTag();
            result = convertView;
        }

        viewHolder.name.setText(dataModel.getName());
        viewHolder.status.setText("Current status: " + dataModel.isCurrentState());
        viewHolder.aSwitch.setChecked(dataModel.isCurrentState());


        // Here we can add listener too.
        // viewHolder.aSwitch.setOnCheckedChangeListener(new -- Listener --);


        return convertView;
    }
}
