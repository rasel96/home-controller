package com.rasel96.homecontroller;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

public class LightBulbFragment extends Fragment {



    ArrayList<DataModel> dataModels;
    ListView listView;
    private static CustomAdapter customAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_light_bulb, container, false);

        // Listing
        listView = rootView.findViewById(R.id.light_bulb_list);
        dataModels = new ArrayList<>();
        dataModels.add(new DataModel("Rasel", true));
        dataModels.add(new DataModel("Ritu", false));
        dataModels.add(new DataModel("Akhi", true));
        dataModels.add(new DataModel("Kenan", false));
        dataModels.add(new DataModel("Nazma", true));
        dataModels.add(new DataModel("Ashraf", false));

        customAdapter = new CustomAdapter(dataModels, getActivity().getApplicationContext());
        listView.setAdapter(customAdapter);

        return rootView;
    }
}
