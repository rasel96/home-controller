package com.rasel96.homecontroller;

public class DataModel {

    private String name;
    private boolean currentState;

    public DataModel(String name, boolean currentState) {
        this.name = name;
        this.currentState = currentState;
    }

    public boolean isCurrentState() {
        return currentState;
    }

    public void setCurrentState(boolean currentState) {
        this.currentState = currentState;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
