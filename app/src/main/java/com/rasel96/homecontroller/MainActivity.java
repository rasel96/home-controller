package com.rasel96.homecontroller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    CardView lightBulbCard, fanCard, doorCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Setting Action Bar Icon
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.home_icon_xml);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        // Finding the Cards
        lightBulbCard = findViewById(R.id.lightBulbCardView);
        fanCard = findViewById(R.id.fanCardView);
        doorCard = findViewById(R.id.doorCardView);

        // Setting the listener
        lightBulbCard.setOnClickListener(cardListener);
        fanCard.setOnClickListener(cardListener);
        doorCard.setOnClickListener(cardListener);

    }

    private View.OnClickListener cardListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MainActivity.this, DetailActivity.class);
            intent.putExtra("id", v.getId());
            startActivity(intent);
        }
    };
}
